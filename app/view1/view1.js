'use strict';

angular.module('myApp.admin', ['ngRoute', 'ui.grid' , 'ui.grid.pagination', 'ui.grid.edit'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/admin', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', function($scope) {
	$scope.gridOptions = {
		editable: true,
		paginationPageSizes: [5, 10, 15],
		paginationPageSize: 5,
		columnDefs: [
		{ field: 'Caps' , enableCellEdit: true},
		{ field: 'Qty' , enableCellEdit: true, type: "number"},
		{ field: 'Price' ,enableCellEdit: true}
		],
		onRegisterApi: function (gridApi) {
		$scope.grid1Api = gridApi;
		
			/**
			* get old and new values 
			*/
		   gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {

			  alert('New Value: ' + newValue + ', Old Value: ' + oldValue);

			  $scope.$apply();

			});
		}
	};

	/* create dummy json for caps*/
	$scope.users = [
	{ Caps: "Puma Long Hat", Qty: 10, Price: '$2.50' },
	{ Caps: "Nike Round Hat", Qty: 30, Price: '$3.50' },
	{ Caps: "Adidas Baseball cap", Qty: 29, Price: '$4.50' },
	{ Caps: "ESPN cricket Cap", Qty: 25, Price: '$6.50' },
	{ Caps: "Sateesh Chandra Cap", Qty: 27, Price: '$7.50' },
	{ Caps: "Nike Woman cap", Qty: 38, Price: '$8.50' },
	{ Caps: "Sudheer Rayana Cap", Qty: 25, Price: '$9.50' },
	{ Caps: "Honey Yemineni Cap", Qty: 7, Price: '$10.50' },
	{ Caps: "Mahendra Dasari Cap", Qty: 22, Price: '$11.50' },
	{ Caps: "Mahesh Dasari Cap", Qty: 23, Price: '$12.50' },
	{ Caps: "Nagaraju Dasari Cap", Qty: 34, Price: '$13.50' },
	{ Caps: "Gopi Krishna Cap", Qty: 29, Price: '$15.50' },
	{ Caps: "Sudheer Uppala Cap", Qty: 19, Price: '$16.50' },
	{ Caps: "Sushmita Cap", Qty: 27, Price: '$18.50' }
	];
	$scope.gridOptions.data = $scope.users;

}]);